from django.conf.urls import url
#from django.conf.urls.defaults import patterns
from . import views
from django.views.generic.base import TemplateView
from django.contrib.staticfiles.urls import static,staticfiles_urlpatterns
from django.conf import settings

# We are adding a URL called /home
urlpatterns = [
    url(r'^dashboard/', views.dashboard, name='dashboard'),
    url(r'^restaurantlogin/', views.restaurantlogin, name='restaurantlogin'),
    url(r'^createMenu/', views.createMenu, name='createMenu'),
    url(r'^ownerMenuList/(\d+)', views.ownerMenuList, name='ownerMenuList'),
    url(r'^deletemenu/(\d+)', views.deletemenu, name='deletemenu'),
    url(r'^ownerTableBooking/(\d+)', views.ownerTableBooking, name='ownerTableBooking'),
    url(r'^$', views.home, name='home')
,
]
#urlpatterns += staticfiles_urlpatterns() #this serves static files and media files.
    #in case media is not served correctly
#urlpatterns += patterns('',
#        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
#            'document_root': settings.MEDIA_ROOT,
#            }),
#    )
#urlpatterns += staticfiles_urlpatterns()
#urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)