from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class States(models.Model):
	state_name=models.CharField(max_length=50)
	def __str__(self):              # __unicode__ on Python 2
		return self.state_name

class Cities(models.Model):
	state=models.ForeignKey(States)
	city_name=models.CharField(max_length=50)
	def __str__(self):              # __unicode__ on Python 2
		return self.city_name

class Locality(models.Model):
	city=models.ForeignKey(Cities)
	locality_name=models.CharField(max_length=50)
	def __str__(self):              # __unicode__ on Python 2
		return self.locality_name

class Cuisine(models.Model):
	description=models.CharField(max_length=100)
	type_name=models.CharField(max_length=50)
	def __str__(self):              # __unicode__ on Python 2
		return self.type_name
	
class Restaurant_Owner(models.Model):
	restaurant_user=models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)
	
class Restaurant(models.Model):
	restaurant_owner=models.ForeignKey(Restaurant_Owner)
	rname=models.CharField(max_length=100)
	street_address=models.CharField(max_length=200)
	locality=models.ForeignKey(Locality)
	description=models.CharField(max_length=200)
	def __str__(self):              # __unicode__ on Python 2
		return self.rname

class Restaurant_Images(models.Model):
	restaurant=models.ForeignKey(Restaurant)
	type_name=models.IntegerField()
	imagecontent=models.ImageField(upload_to='site_media')
	# def save(self, *args, **kwargs):
	# 	try:
	# 		path1 = safe_join(os.path.abspath(settings.MEDIA_ROOT)+'\images', self.imagecontent)
	# 		image_file = open(path1,'rb')
	# 		file_content = image_file.read()
	# 		self.imagecontent=file_content
	# 	except:
	# 		filename = 'no_image.png'
	# 		path = safe_join(os.path.abspath(settings.MEDIA_ROOT), filename)
	# 		#if not os.path.exists(path):
	# 		#    raise ObjectDoesNotExist
	# 		no_image = open(path, 'rb')
	# 		file_content = no_image.read()
	# 	super(Categories, self).save(*args, **kwargs)
    

class Food_Timings(models.Model):
	name=models.CharField(max_length=100)
	description=models.CharField(max_length=100)
	def __str__(self):              # __unicode__ on Python 2
		return self.name

class RestaurantFoodTiming(models.Model):
	restaurant=models.ForeignKey(Restaurant)
	food_timings=models.ForeignKey(Food_Timings)
	from_T=models.TimeField()
	to_T=models.TimeField()
	def __str__(self):              # __unicode__ on Python 2r
		return self.restaurant.rname + " "+ self.food_timings.name

class Floor(models.Model):
	restaurant=models.ForeignKey(Restaurant)
	floorno=models.IntegerField()
	def __str__(self):              # __unicode__ on Python 2r
		return self.restaurant.rname + " "+ self.floorno

class FloorPlan(models.Model):
	floor=models.ForeignKey(Floor)
	point=models.IntegerField()
	next_point=models.IntegerField()
	x=models.FloatField()
	y=models.FloatField()
	
class Menu(models.Model):
	name=models.CharField(max_length=100)
	description=models.CharField(max_length=200)
	food_timings=models.ForeignKey(Food_Timings)
	restaurant=models.ForeignKey(Restaurant)
	Veg=models.BooleanField(default=True)
	cuisine=models.ForeignKey(Cuisine)
	price=models.FloatField()
	def __str__(self):              # __unicode__ on Python 2
		return self.name
