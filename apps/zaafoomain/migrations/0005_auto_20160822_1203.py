# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-22 12:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zaafoomain', '0004_auto_20160820_0932'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant_images',
            name='imagecontent',
            field=models.ImageField(upload_to='site_media'),
        ),
    ]
