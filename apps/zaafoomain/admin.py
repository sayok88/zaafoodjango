from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Cities)
admin.site.register(Locality)
admin.site.register(States)
admin.site.register(Cuisine)
admin.site.register(Restaurant)
admin.site.register(Restaurant_Images)
admin.site.register(Food_Timings)
admin.site.register(RestaurantFoodTiming)
admin.site.register(Restaurant_Owner)
admin.site.register(Menu)

