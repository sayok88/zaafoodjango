# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-29 10:06
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('zaafoomain', '0006_floor_floorplan'),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=200)),
                ('Veg', models.BooleanField(default=True)),
                ('cuisine', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='zaafoomain.Cuisine')),
                ('food_timings', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='zaafoomain.Food_Timings')),
                ('restaurant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='zaafoomain.Restaurant')),
            ],
        ),
    ]
