from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.forms import ModelForm
from .models import *


from django.utils import html

class LoginForm(AuthenticationForm):
    """Login form."""

    username = forms.CharField(
        label="Username",
        max_length=30,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(
        label="Password",
        max_length=30,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'name': 'password'}))
class MenuForm(ModelForm):
    """docstring for MenuForm"""
    class Meta:
        model=Menu
        fields=('name','description','food_timings','restaurant','Veg','cuisine','price')