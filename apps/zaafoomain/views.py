from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime, timedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate,login
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404
# Create your views here.
def home(request):
    """Home page."""
    return render(request, "zaafoomain/home.html")

def restaurantlogin(request):
    
    if request.method == 'POST':
    	form2=LoginForm(request.POST)
    	print(request.POST)
    	print(form2)
    	 
    	username = request.POST['username']
    	password = request.POST['password']
    	print(username)
    	user = authenticate(username=username, password=password)
    	if user is not None:
        	login(request,user)
        	print(username)
        	resowner=Restaurant_Owner.objects.get(restaurant_user=user)
        	if resowner is not None:
        		return HttpResponseRedirect('/dashboard')
    form1=LoginForm()
    return render(request,"zaafoomain/restaurantlogin.html",{'form': form1})
@login_required
def dashboard(request):
	user=request.user
	resowner=Restaurant_Owner.objects.get(restaurant_user=user)

	restsow=Restaurant.objects.filter(restaurant_owner=resowner)
	
	print(restsow)
	restimages=Restaurant_Images.objects.filter(restaurant__in=restsow,type_name=0).select_related()
	print(restimages)
	return render(request,'zaafoomain/dashboard.html',{'restsowned':restimages})
@login_required
def ownerMenuList(request,restid):
	resto=Restaurant.objects.filter(id=restid)
	menus=Menu.objects.filter(restaurant=resto)
	return render(request,'zaafoomain/ownermenulist.html',{'resmenu':menus})
@login_required
def createMenu(request):
	if request.method=='POST':
		form2=MenuForm(request.POST)
		print(request.POST)
		print(form2)
		if form2.is_valid():
			form2.save()
			return HttpResponseRedirect('/dashboard') #add menulist redirect
	form2=MenuForm()
	user=request.user
	print(user)
	resowner=Restaurant_Owner.objects.get(restaurant_user=user)

	restsow=Restaurant.objects.filter(restaurant_owner=resowner)
	form2.fields['restaurant'].queryset=restsow
	return render(request,"zaafoomain/createmenu.html",{'form': form2})

@login_required
def deletemenu(request,menuid):
	menu=get_object_or_404(Menu,pk=menuid)
	menu.delete()
	return HttpResponseRedirect('/dashboard') #add menulist redirect
@login_required
def ownerTableBooking(request,restid):
	resto=Restaurant.objects.filter(id=restid)
	menus=Menu.objects.filter(restaurant=resto)
	return render(request,'zaafoomain/resbook.html',{'resmenu':menus})
